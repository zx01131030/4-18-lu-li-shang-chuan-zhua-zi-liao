package sample;

import com.speedment.runtime.core.ApplicationBuilder;
import database.TestApplication;
import database.TestApplicationBuilder;
import database.test.test.me.Me;
import database.test.test.me.MeImpl;
import database.test.test.me.MeManager;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebEngine;

import java.util.Optional;

public class Controller {
    public Button btnOK;
    public HTMLEditor txtAboutMe;
    public javafx.scene.web.WebView web;
    public Button doGetAboutMe;

    public void onGetReadMe(ActionEvent actionEvent) {
        System.out.println(txtAboutMe.getHtmlText());

        TestApplication testApplication = new TestApplicationBuilder().withUsername("root")
                .withPassword("abcd1234").withLogging(ApplicationBuilder.LogType.PERSIST)
                .withLogging(ApplicationBuilder.LogType.CONNECTION)
                .withLogging(ApplicationBuilder.LogType.STREAM).build();

        MeManager meManager = testApplication.getOrThrow(MeManager.class);
        meManager.persist(new MeImpl().setAboutme(txtAboutMe.getHtmlText()));

    }

    public void onGetAboutMe(ActionEvent actionEvent) {
        TestApplication testApplication = new TestApplicationBuilder().withUsername("root")
                .withPassword("abcd1234").withLogging(ApplicationBuilder.LogType.PERSIST)
                .withLogging(ApplicationBuilder.LogType.CONNECTION)
                .withLogging(ApplicationBuilder.LogType.STREAM).build();

        MeManager meManager = testApplication.getOrThrow(MeManager.class);

        Optional<Me> meOptional = meManager.stream().findFirst();
        WebEngine webEngine = web.getEngine();
        if (meOptional.isPresent()) {
            webEngine.loadContent(meOptional.get().getAboutme(), "text/html");

        } else {
            webEngine.load("https://www.google.com");
        }

    }
}
