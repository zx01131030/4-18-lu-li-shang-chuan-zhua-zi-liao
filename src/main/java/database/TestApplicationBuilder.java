package database;

import database.generated.GeneratedTestApplicationBuilder;

/**
 * The default {@link com.speedment.runtime.core.ApplicationBuilder}
 * implementation class for the {@link com.speedment.runtime.config.Project}
 * named test.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class TestApplicationBuilder extends GeneratedTestApplicationBuilder {}