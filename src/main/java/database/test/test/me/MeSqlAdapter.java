package database.test.test.me;

import database.test.test.me.generated.GeneratedMeSqlAdapter;

/**
 * The SqlAdapter for every {@link database.test.test.me.Me} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public class MeSqlAdapter extends GeneratedMeSqlAdapter {}