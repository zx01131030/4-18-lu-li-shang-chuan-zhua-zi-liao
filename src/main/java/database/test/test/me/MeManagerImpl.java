package database.test.test.me;

import database.test.test.me.generated.GeneratedMeManagerImpl;

/**
 * The default implementation for the manager of every {@link
 * database.test.test.me.Me} entity.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class MeManagerImpl 
extends GeneratedMeManagerImpl 
implements MeManager {}