package database.test.test.me;

import database.test.test.me.generated.GeneratedMeImpl;

/**
 * The default implementation of the {@link database.test.test.me.Me}-interface.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public final class MeImpl 
extends GeneratedMeImpl 
implements Me {}