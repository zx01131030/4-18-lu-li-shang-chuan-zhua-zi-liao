package database.test.test.me;

import database.test.test.me.generated.GeneratedMe;

/**
 * The main interface for entities of the {@code me}-table in the database.
 * <p>
 * This file is safe to edit. It will not be overwritten by the code generator.
 * 
 * @author company
 */
public interface Me extends GeneratedMe {}